package waarkoop.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Detail{

	@JsonProperty("served_by")
	private String servedBy;

	@JsonProperty("error")
	private boolean isError;

	@JsonProperty("message")
	private String message;

	@JsonProperty("requested_item")
	private String requestedItem;
}