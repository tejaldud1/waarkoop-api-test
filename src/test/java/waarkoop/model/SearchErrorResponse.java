package waarkoop.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchErrorResponse{

	@JsonProperty("detail")
	private Detail detail;
}