package waarkoop.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import waarkoop.model.SearchErrorResponse;
import waarkoop.model.SearchResponseItem;
import waarkoop.service.SearchProductAPI;

import java.util.List;
import java.util.stream.Collectors;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class SearchStepDefinitions {
    @Steps
    public SearchProductAPI searchProductAPI;

    @Steps
    public ResponseValidationSteps responseValidationSteps;
    Response searchResultsResponse;
    List<SearchResponseItem> searchList;

    @When("(.*) search for (.*)$")
    public void heCallsEndpointForSearch(String user, String product_name) {
        searchResultsResponse = searchProductAPI.getSearchResults(product_name);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForApple(String product_name) {
        List<SearchResponseItem> searchList = searchResultsResponse.as(new TypeRef<List<SearchResponseItem>>() {
        });
        List<String> titleList = searchList.stream().map(se -> se.getTitle().toLowerCase()).collect(Collectors.toList());
        assertThat("Search Results are not as expected: ", titleList, (everyItem(containsString(product_name))));
    }

    @Then("validate that search results are displayed for {string}")
    public void validateThatSearchResultsDisplayedFor(String product_name) {
        List<SearchResponseItem> searchList = searchResultsResponse.as(new TypeRef<List<SearchResponseItem>>() {
        });
        List<String> invalidTitles = searchList.stream().map(se -> se.getTitle().toLowerCase()).filter(title ->
                !title.contains(product_name)).collect(Collectors.toList());
        assertThat("Search Results are not as expected, none matching items are :" + invalidTitles, invalidTitles.isEmpty(), is(true));
    }

    @Then("user should get an error message as {string} and does not see search results")
    public void userShouldGetAnErrorMessageAsAndDoesNotSeeSearchResults(String errorMessage) {
        SearchErrorResponse errorResponse = searchResultsResponse.as(SearchErrorResponse.class);
        assertThat("Error Message is not as expected", errorResponse.getDetail().getMessage(), is(errorMessage));
        assertThat(errorResponse.getDetail().isError(), is(true));
    }

    @Then("the response code should be {int}")
    public void theResponseCodeShouldBe(int responseCode) {
        responseValidationSteps.validateResponseCode(responseCode);
    }

    @And("validate response has required fields")
    public void validateResponseHasRequiredFields() {
        searchList = searchResultsResponse.as(new TypeRef<List<SearchResponseItem>>() {
        });
        assertThat("No results fetched",searchList.isEmpty(), is(false));
        searchList.forEach(searchItem-> {
            assertThat("Price is not populated in response",searchItem.getPrice(), notNullValue());
            assertThat("Unit is not populated in response",searchItem.getUnit(), notNullValue());
        });
    }
}
