package waarkoop.stepdefinitions;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ResponseValidationSteps {

    @Step
    public void validateResponseCode(int statusCode)
    {
        SerenityRest.restAssuredThat(response -> response.statusCode(statusCode));
    }
}
