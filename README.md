# Project Title
This is an Waarkoop API Automation Project using Serenity and Cucumber.

## Getting Started
These instructions will guide you on
- How to install, run tests with different parameters
- How to check Report on local and on Gitlab CI
- Steps to add new tests
- Project refactoring points

## Built With
* [Maven](https://maven.apache.org/) - Dependency Management
* [Serenity](http://thucydides.info/docs/serenity-staging/) - Serenity BDD is an open source library for test automation.
* [Java](https://www.oracle.com/java/) - Programming Language

## How to install and run tests
This project gives you a basic project setup, along with some automation of product search API scenarios and supporting classes.

#### Go to terminal and Checkout project using below command
    git checkout https://gitlab.com/tejaldud1/waarkoop-api-test.git

#### Go to project root folder 
    cd waarkoop-api-test

## Executing the tests from local 
To run the test, you can either just run the `TestRunner` test runner class, or run `mvn clean verify` from the command line.

## Accesing tests reports

The test results will be recorded in the `target/site/serenity` directory.
Go to above path and open index.html 

## Accessing reports from Gitlab CI

- Go to https://gitlab.com/tejaldud1/waarkoop-api-test.git
- Navigate CI/CD > Pipelines > Click on pipeline number >Click Jobs tab > Click Report under Report stage > Click Browse button under Job artifacts


  ![Screenshot](src/test/resources/report_screenshots/gitlab_report_browse.png)


- Click public path then click HTML file index.html The report will be displayed.


  ![Screenshot](src/test/resources/report_screenshots/report.png)

### Environment-specific configurations

We can also configure environment-specific properties and options, so that the tests can be run in different environments. 
Here, we configure three environments, __dev__, _staging_ and _prod_, with different starting URLs for each:

```json
environments {
  default {
  restapi.baseurl="https://waarkoop-server.herokuapp.com"
}
dev {
  restapi.baseurl="https://waarkoop-server.herokuapp.com"
}
staging {
  restapi.baseurl="https://waarkoop-server.herokuapp.com"
}
prod {
  restapi.baseurl="https://waarkoop-server.herokuapp.com"
}
}
```

You use the `environment` system property to determine which environment to run against. For example to run the tests in the staging environment, you could run:
```json
$ mvn clean verify -Denvironment=staging
```
### The project directory structure
The project has build scripts for Maven, and follows the standard directory structure used in most Serenity projects:

```Gherkin
src
  + main
  + test
    + java                         Test runners and supporting code
    + resources
      + features                   Feature files
        + search                   Feature file subdirectories
            search_product.feature
```

## Steps to Automate New Test Cases

Please follow existing folder structure to add new tests, In order to automate new endpoint below steps can be followed.

 - Create Feature file and add respective test scenarios to e.g search_product.feature

In this scenario, User is searching by product name and validating the API response.

```Gherkin
Feature: Search for the products

  Scenario Outline:Search for the product and validate the search results
    When User search for <product_name>
    Then the response code should be 200
    And validate response has required fields
    Then validate that search results are displayed for "<product_name>"
    Examples:
      | product_name |
      | apple        |
```
 - Create the API class implemenation under specified package e.g. SearchProductAPI
 - Create Pojos classes if required to validate the response e.g SearchResponseItem
 - Create Step implementation for specific endpint and response validation under package src/test/java/waarkoop/stepdefinitions e.g. SearchStepDefinitions
```java

    @When("(.*) search for (.*)$")
    public void heCallsEndpointForSearch(String user, String product_name) {
        searchResultsResponse = searchProductAPI.getSearchResults(product_name);
    }

```
## Project refactoring points considered as below 

1. Clean up  project to have a single build system as maven - this is to simplify the project and avoid confusion related to active build system.
2. Removed unused dependencies from maven - clean up activity and reducing build time
3. Refactored Feature file : Implemented in a way so that steps can be reused and also make it more readable from the end user point of view
   - Rewarded scenario steps
   - Parameterized product name (generic step implementation)
   - Added response validation steps
   - Added scenario description 
   - Fixed Report Generation issue
5. Added Pojos to map API response - Encapsulating response as java object for Response validation/assertion
6. Added API class - Easy to maintain the code for each service  and code reusability
7. Externalized rest api url to property file 
8. Uploaded project on Gitlab and configured CI/CD pipleline - build and test can be triggered on every code merge/commit and get early feedback
